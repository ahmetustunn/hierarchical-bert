# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from transformers import BertModel, BertPreTrainedModel


class HierarchicalBertForBinaryClassification(BertPreTrainedModel):

    def __init__(self, config):
        super(HierarchicalBertForBinaryClassification, self).__init__(config)
        self.num_labels = config.num_labels

        self.bert = BertModel(config)
        self.dropout = nn.Dropout(config.hidden_dropout_prob)
        self.classifier = nn.Linear(config.hidden_size, 1)
        self.loss_fct = nn.BCEWithLogitsLoss()

        self.attn_P = nn.Linear(config.hidden_size, config.hidden_size)
        self.attn_W = nn.Linear(config.hidden_size, 1, bias=False)

        self.init_weights()

    def _init_attn(self):
        torch.nn.init.xavier_uniform_(self.attn_P.weight)
        torch.nn.init.xavier_uniform_(self.attn_W.weight)

    def forward(self, input_ids, token_type_ids=None, attention_mask=None, labels=None,
                position_ids=None, head_mask=None, text_num=None):

        b,s,l = input_ids.shape
        outputs = self.bert(input_ids=input_ids.view(b*s,l),
                            position_ids=position_ids,
                            token_type_ids=token_type_ids.view(b*s,l),
                            attention_mask=attention_mask.view(b*s,l),
                            head_mask=head_mask)[1]
        outputs = outputs.view(b, s, self.config.hidden_size)

        maxlen = s
        attn_mask = torch.arange(maxlen)[None, :] < text_num[:, None].to(torch.device('cpu'))

        out_p = torch.tanh(self.attn_P(outputs))
        attn_w = self.attn_W(out_p).squeeze(-1)
        attn_w[~attn_mask] = float(-100000)
        attn_w = F.softmax(attn_w, dim=1)
        attended = outputs * attn_w.unsqueeze(-1)
        out = attended.sum(1, True).squeeze(1)
        logits = self.classifier(out)

        if labels is not None:
            loss = self.loss_fct(logits.squeeze(1), labels.float())

        return loss, logits


class BertForBinaryClassification(BertPreTrainedModel):

    def __init__(self, config):
        super(BertForBinaryClassification, self).__init__(config)
        self.num_labels = config.num_labels

        self.bert = BertModel(config)
        self.dropout = nn.Dropout(config.hidden_dropout_prob)
        self.classifier = nn.Linear(config.hidden_size, 1)
        self.loss_fct = nn.BCEWithLogitsLoss()
        self.init_weights()

    def forward(self, input_ids=None, attention_mask=None, token_type_ids=None,
                position_ids=None, head_mask=None, inputs_embeds=None, labels=None, text_num=None):

        outputs = self.bert(input_ids=input_ids, position_ids=position_ids, token_type_ids=token_type_ids,
                            attention_mask=attention_mask, head_mask=head_mask)

        last_hidden = outputs[1]

        logits = self.classifier(last_hidden)

        if labels is not None:
            loss = self.loss_fct(logits.squeeze(1), labels.float())

        return loss, logits
