import torch
from torch.utils.data import Dataset


class CaseDataset(Dataset):
    def __init__(self, features, max_seq_len, max_text_num):
        self.features = features
        self.max_seq_len = max_seq_len
        self.max_text_num = max_text_num

    def __len__(self):
        return len(self.features)

    def __getitem__(self, index):
        features = self.features[index]
        case_features = features.case_features
        label_id = features.label_id

        num_texts = len(case_features) if len(case_features) < self.max_text_num else self.max_text_num

        input_ids = torch.zeros((self.max_text_num, self.max_seq_len), dtype=torch.long)
        input_ids[:num_texts] = torch.transpose(torch.tensor(case_features, dtype=torch.long)[:num_texts], 0,1)[0]

        attention_mask = torch.zeros((self.max_text_num, self.max_seq_len), dtype=torch.long)
        attention_mask[:num_texts] = torch.transpose(torch.tensor(case_features, dtype=torch.long)[:num_texts], 0, 1)[1]

        token_type_ids = torch.zeros((self.max_text_num, self.max_seq_len), dtype=torch.long)
        token_type_ids[:num_texts] = torch.transpose(torch.tensor(case_features, dtype=torch.long)[:num_texts], 0, 1)[2]

        labels = torch.tensor(label_id, dtype=torch.long)
        num_text_tensor = torch.tensor(num_texts, dtype=torch.long)

        return input_ids, attention_mask, token_type_ids, labels, num_text_tensor

