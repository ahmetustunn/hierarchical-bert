#!/bin/bash

#SBATCH --time=24:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem=60G
#SBATCH --cpus-per-task=4
#SBATCH --job-name=bert.1
#SBATCH --output=bert.1.log
#SBATCH --partition=gpu
#SBATCH --gres=gpu:v100:1

module load Python/3.6.4-intel-2018a
python run_classifier.py --task_name case --case_field 'facts' --data_dir data --model_type bert --model_name_or_path bert-base-cased --output_dir bert.out.1 --do_train --do_eval --overwrite_output_dir --overwrite_cache --per_gpu_train_batch_size 8 --max_seq_length 512 --save_steps 400 --logging_steps 400 --num_train_epochs 3
python run_classifier.py --task_name case --case_field 'facts' --data_dir data --model_type h-bert --model_name_or_path bert-base-cased --output_dir h-bert.out.1 --do_train --do_eval --overwrite_output_dir --overwrite_cache --per_gpu_train_batch_size 8 --max_seq_length 128 --save_steps 400 --logging_steps 400 --max_text_num 8 --num_train_epochs 3 --attn_learning_rate 5e-4